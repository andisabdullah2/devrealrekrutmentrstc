<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam_User extends Model
{
    use HasFactory;
    protected $table = "exam_user";
    public $timestamps = false;
}
