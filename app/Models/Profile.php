<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $table = "profile";
    protected $fillable = [
        'user_id',
        'tgl_lahir',
        'alamat',
        'pendidikan',
        'jenis_kelamin',
        'agama',
        'nomorTes',
        'no_hp',
        'nik',
        'nama_wali',
        'no_hpWali',
    ];
}
