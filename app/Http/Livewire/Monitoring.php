<?php

namespace App\Http\Livewire;
use Carbon\Carbon;
use App\Models\Exam;
use App\Models\User;
use App\Models\ExamSubject;
use App\Models\Exam_User;
use App\Models\Audio;
use App\Models\Image;
use App\Models\Video;
use Livewire\Component;
use App\Models\Document;
use App\Models\Question;
use App\Models\Subject;
use Livewire\WithPagination;
use Illuminate\Contracts\Session\Session;
use Illuminate\Database\Eloquent\Builder;

class Monitoring extends Component
{
    use WithPagination;


    public function mount($id)
    {
        $this->exam_id = $id;
    }


    public function render()
    {
        $currentDateTime = Carbon::now();
        $examsforid = Exam::findOrFail($this->exam_id);
        $exams_id = $examsforid->id;
        $exams = Exam::join('exam_user', 'exams.id', '=', 'exam_user.exam_id')
                    ->join('users', 'users.id', '=', 'exam_user.user_id')
                    ->join('exam_subject', 'exam_subject.id', '=', 'exam_user.exam_subject_id')
                    ->join('subjects', 'subjects.id', '=', 'exam_subject.subject_id')
                    ->where('exams.id', '=', $exams_id)
                    ->select('exams.*', 'users.name as user_name', 'subjects.name as subject_name','exam_user.score as score')
                    ->orderByDesc('score')
                    ->get();
        
        foreach ($exams as $exam) {
            if ($currentDateTime >= $exam->start && $currentDateTime <= $exam->end) {
                $scoreSementara = $exam->score;
            } else {
                $scoreSementara = null;
            }
            if ($currentDateTime > $exam->end) {
                $scoreAkhir = $exam->score;
            } else {
                $scoreAkhir = null;
            }
            $exam->score_sementara = $scoreSementara;
            $exam->score_akhir = $scoreAkhir;
        }

        $examskeseluruhan_result = Exam::join('exam_user', 'exams.id', '=', 'exam_user.exam_id')
        ->join('users', 'users.id', '=', 'exam_user.user_id')
        ->join('exam_subject', 'exam_subject.id', '=', 'exam_user.exam_subject_id')
        ->join('subjects', 'subjects.id', '=', 'exam_subject.subject_id')
        ->select('exams.*', 'users.name as user_name', 'subjects.name as subject_name', 'exam_user.score as score')
        ->orderByDesc('score')
        ->get();

        foreach ($examskeseluruhan_result as $examskeseluruhan) {
            if ($currentDateTime >= $examskeseluruhan->start && $currentDateTime <= $examskeseluruhan->end) {
            $scoreSementara = $examskeseluruhan->score;
            } else {
            $scoreSementara = null;
            }
            if ($currentDateTime > $examskeseluruhan->end) {
            $scoreAkhir = $examskeseluruhan->score;
            } else {
            $scoreAkhir = null;
            }
            $examskeseluruhan->score_sementara = $scoreSementara;
            $examskeseluruhan->score_akhir = $scoreAkhir;
        }
        return view('livewire.monitoring', [
            'exams' => $exams,
            'examskeseluruhan_result' =>$examskeseluruhan_result,
        ]);
    }

}
