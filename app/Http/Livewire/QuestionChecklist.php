<?php

namespace App\Http\Livewire;

use App\Models\Exam;
use App\Models\Subject;
use Livewire\Component;
use App\Models\Question;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;

class QuestionChecklist extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $q = null;
    public $p = null;
    public $selectedQuestion = [];
    public $question_list = [];

    public function mount($selectedExam = null)
    {
        if (is_null($selectedExam)) {
            $this->selectedQuestion = [];
        } else {
            $this->selectedQuestion = Exam::findOrFail($selectedExam)->subject()->pluck('subject_id')->toArray();
        }
       
    }

    public function deselectQuestion($questionId)
    {
        if (($key = array_search($questionId, $this->selectedQuestion)) !== false) {
            unset($this->selectedQuestion[$key]);
        }
    }

    public function updatedSelectedQuestion()
    {

        $this->dispatchBrowserEvent('question-updated', ['selectedQuestion' => $this->selectedQuestion]);
    }

    public function render()
    {
        $subjects = Subject::paginate(5);
        $question = new Question();
        if (empty($this->selectedQuestion)) {
            return view('livewire.question-checklist', compact('subjects','question'));
        } else {
            $questionsAll = Subject::latest()->whereIn('id', $this->selectedQuestion)->get();
            return view('livewire.question-checklist', compact('subjects', 'questionsAll','question'));
        }
    }  
}
