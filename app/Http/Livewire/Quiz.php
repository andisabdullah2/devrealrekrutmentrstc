<?php

namespace App\Http\Livewire;

use App\Models\Exam;
use App\Models\User;
use App\Models\ExamSubject;
use App\Models\Exam_User;
use App\Models\Audio;
use App\Models\Image;
use App\Models\Video;
use Livewire\Component;
use App\Models\Document;
use App\Models\Question;
use App\Models\Subject;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Builder;

class Quiz extends Component
{
    use WithPagination;

    public $page = 1;
    protected $paginationTheme = 'bootstrap';
    public $exam_id;
    public $user_id;
    public $selectedAnswers = [];
    public $total_question;
    public $tkd_questions;
    protected $listeners = ['endTimer' => 'submitAnswers'];

    public function mount($id)
    {
        $this->exam_id = $id;
        
    }

    public function nextPage()
    {
        $currentPage = $this->questions()->currentPage();
        $nextPage = $currentPage + 1;

        // Redirect to the next page
        return redirect()->route('exams.start', ['id' => $this->exam_id, 'page' => $nextPage]);
    }


    public function questions()
    {
        $currentUser = User::findOrFail(Auth()->id());
        $exam = Exam::findOrFail($this->exam_id);
        $idUser = $currentUser->id;
        $exam_user = Exam_User::where('exam_id', $exam->id)->where('user_id', $idUser)->pluck('exam_subject_id')->toArray();
        $exam_subject = ExamSubject::where('exam_id', $exam->id)->where('id', $exam_user)->pluck('subject_id')->toArray();
        $exam_questions = Question::where('subject_id', $exam_subject);
        $tkd_questions = clone $exam_questions;
        $this->tkd_questions = $tkd_questions->where('kategori','TKD')->count();
        $this->total_question = $exam_questions->count();
        if ($this->total_question >= $exam->total_question) {
            $questions = $exam_questions->take($exam->total_question)->paginate(1);
        } elseif ($this->total_question < $exam->total_question) {
            $questions = $exam_questions->take($this->total_question)->paginate(1);
        }
        return $questions;
    }
    

    public function answers($questionId, $option)
    {
        $this->selectedAnswers[$questionId] = $questionId.'-'.$option;
    }

    public function submitAnswers()
    {
        if(!empty($this->selectedAnswers))
        {
            
            $score = 0;
            foreach($this->selectedAnswers as $key => $value)
            {
                $userAnswer = "";
                $rightAnswer = Question::findOrFail($key)->answer;
                $userAnswer = substr($value, strpos($value,'-')+1);
                $answerA = Question::findOrFail($key)->option_A;
                $answerB = Question::findOrFail($key)->option_B;
                $answerC = Question::findOrFail($key)->option_C;
                $answerD = Question::findOrFail($key)->option_D;
                $answerE = Question::findOrFail($key)->option_E;
                $bobotA = Question::findOrFail($key)->bobotA;
                $bobotB = Question::findOrFail($key)->bobotB;
                $bobotC = Question::findOrFail($key)->bobotC;
                $bobotD = Question::findOrFail($key)->bobotD;
                $bobotE = Question::findOrFail($key)->bobotE;
                $bobot = 100 / $this->tkd_questions;
                if($userAnswer == $rightAnswer){
                    $score = $score + $bobot;
                }
                elseif ($userAnswer == $answerA) {
                    $score = $score + $bobotA;
                }
                elseif($userAnswer == $answerB) {
                    $score = $score + $bobotB;
                }
                elseif($userAnswer == $answerC) {
                    $score = $score + $bobotC;
                }
                elseif($userAnswer == $answerD) {
                    $score = $score + $bobotD;
                }
                elseif($userAnswer == $answerE) {
                    $score = $score + $bobotE;
                }
            }
        }else{
            $score = 0;
        }
        
        $selectedAnswers_str = json_encode($this->selectedAnswers);
        $this->user_id = Auth()->id();
        $user = User::findOrFail($this->user_id);
        $user_exam = $user->whereHas('exams', function (Builder $query) {
            $query->where('exam_id',$this->exam_id)->where('user_id',$this->user_id);
        })->count();
        if($user_exam == 0)
        {
            $user->exams()->attach($this->exam_id, ['history_answer' => $selectedAnswers_str, 'score' => $score]);
        } else{
            $user->exams()->updateExistingPivot($this->exam_id, ['history_answer' => $selectedAnswers_str, 'score' => $score]);
        }
        
        return redirect()->route('exams.result', [$score, $this->user_id, $this->exam_id]);
    }

    public function nextAnswers()

    {
        if (!empty($this->selectedAnswers)) {
            $score = 0;
            foreach ($this->selectedAnswers as $key => $value) {
                $userAnswer = "";
                $rightAnswer = Question::findOrFail($key)->answer;
                $userAnswer = substr($value, strpos($value, '-') + 1);
                $answerA = Question::findOrFail($key)->option_A;
                $answerB = Question::findOrFail($key)->option_B;
                $answerC = Question::findOrFail($key)->option_C;
                $answerD = Question::findOrFail($key)->option_D;
                $answerE = Question::findOrFail($key)->option_E;
                $bobotA = Question::findOrFail($key)->bobotA;
                $bobotB = Question::findOrFail($key)->bobotB;
                $bobotC = Question::findOrFail($key)->bobotC;
                $bobotD = Question::findOrFail($key)->bobotD;
                $bobotE = Question::findOrFail($key)->bobotE;
                $bobot = 100 / $this->tkd_questions;
                if ($userAnswer == $rightAnswer) {
                    $score = $score + $bobot;
                }
                elseif ($userAnswer == $answerA) {
                    $score = $score + $bobotA;
                }
                elseif($userAnswer == $answerB) {
                    $score = $score + $bobotB;
                }
                elseif($userAnswer == $answerC) {
                    $score = $score + $bobotC;
                }
                elseif($userAnswer == $answerD) {
                    $score = $score + $bobotD;
                }
                elseif($userAnswer == $answerE) {
                    $score = $score + $bobotE;
                }
            }
        } else {
            $score = 0;
        }

        $this->user_id = Auth()->id();
        $user = User::findOrFail($this->user_id);
        $user_exam = $user->whereHas('exams', function (Builder $query) {
            $query->where('exam_id', $this->exam_id)->where('user_id', $this->user_id);
        })->count();

        // Ambil history_answer yang sudah ada
        $existingAnswers = $user->exams()->where('exam_id', $this->exam_id)->where('user_id', $this->user_id)->pluck('history_answer')->first();

        if ($user_exam == 0) {
            // Jika tidak ada data, tambahkan baru
            $user->exams()->attach($this->exam_id, ['history_answer' => json_encode($this->selectedAnswers), 'score' => $score]);
        } else {
            // Jika sudah ada data, gabungkan selectedAnswers dengan existingAnswers
            $existingAnswersArray = $existingAnswers ? json_decode($existingAnswers, true) : [];

            // Loop melalui selectedAnswers dan update jawaban pada pertanyaan yang sudah ada di existingAnswers
            foreach ($this->selectedAnswers as $key => $value) {
                $questionId = explode('-', $value)[0];
                if (array_key_exists($questionId, $existingAnswersArray)) {
                    $existingAnswersArray[$questionId] = $value;
                }
            }

            $user->exams()->updateExistingPivot($this->exam_id, ['history_answer' => json_encode($existingAnswersArray), 'score' => $score]);
        }
        $currentPage = $this->questions()->currentPage();
        $nextPage = $currentPage + 1;
    
        // Redirect ke halaman berikutnya dengan menyertakan selectedAnswers pada URL
        return redirect()->route('exams.start', ['id' => $this->exam_id, 'page' => $nextPage, 'selectedAnswers' => json_encode($this->selectedAnswers)]);
    }

    public function submitAnswersAkhlak()
    {
        if (!empty($this->selectedAnswers)) {
            $score = 0;
            foreach ($this->selectedAnswers as $key => $value) {
                $userAnswer = "";
                $rightAnswer = Question::findOrFail($key)->answer;
                $answerA = Question::findOrFail($key)->option_A;
                $answerB = Question::findOrFail($key)->option_B;
                $answerC = Question::findOrFail($key)->option_C;
                $answerD = Question::findOrFail($key)->option_D;
                $answerE = Question::findOrFail($key)->option_E;
                $bobotA = Question::findOrFail($key)->bobotA;
                $bobotB = Question::findOrFail($key)->bobotB;
                $bobotC = Question::findOrFail($key)->bobotC;
                $bobotD = Question::findOrFail($key)->bobotD;
                $bobotE = Question::findOrFail($key)->bobotE;
                $userAnswer = substr($value, strpos($value, '-') + 1);
                if ($userAnswer == $rightAnswer) {
                    $score = $score + $bobotA;
                }
                elseif($userAnswer == $answerB) {
                    $score = $score + $bobotB;
                }
                elseif($userAnswer == $answerC) {
                    $score = $score + $bobotC;
                }
                elseif($userAnswer == $answerD) {
                    $score = $score + $bobotD;
                }
                elseif($userAnswer == $answerE) {
                    $score = $score + $bobotE;
                }
            }
        } else {
            $score = 0;
        }
        
        $selectedAnswers_str = json_encode($this->selectedAnswers);
        $this->user_id = Auth()->id();
        $user = User::findOrFail($this->user_id);
        $user_exam = $user->whereHas('exams', function (Builder $query) {
            $query->where('exam_id',$this->exam_id)->where('user_id',$this->user_id);
        })->count();
        if($user_exam == 0)
        {
            $user->exams()->attach($this->exam_id, ['history_answer' => $selectedAnswers_str, 'score' => $score]);
        } else{
            $user->exams()->updateExistingPivot($this->exam_id, ['history_answer' => $selectedAnswers_str, 'score' => $score]);
        }
        
        return redirect()->route('exams.result', [$score, $this->user_id, $this->exam_id]);
    }

    public function nextAnswersAkhlak()
    {
        if (!empty($this->selectedAnswers)) {
            $score = 0;
            foreach ($this->selectedAnswers as $key => $value) {
                $userAnswer = "";
                $rightAnswer = Question::findOrFail($key)->answer;
                $answerA = Question::findOrFail($key)->option_A;
                $answerB = Question::findOrFail($key)->option_B;
                $answerC = Question::findOrFail($key)->option_C;
                $answerD = Question::findOrFail($key)->option_D;
                $answerE = Question::findOrFail($key)->option_E;
                $bobotA = Question::findOrFail($key)->bobotA;
                $bobotB = Question::findOrFail($key)->bobotB;
                $bobotC = Question::findOrFail($key)->bobotC;
                $bobotD = Question::findOrFail($key)->bobotD;
                $bobotE = Question::findOrFail($key)->bobotE;
                $userAnswer = substr($value, strpos($value, '-') + 1);
                if ($userAnswer == $rightAnswer) {
                    $score = $score + $bobotA;
                }
                elseif($userAnswer == $answerB) {
                    $score = $score + $bobotB;
                }
                elseif($userAnswer == $answerC) {
                    $score = $score + $bobotC;
                }
                elseif($userAnswer == $answerD) {
                    $score = $score + $bobotD;
                }
                elseif($userAnswer == $answerE) {
                    $score = $score + $bobotE;
                }
            }
        } else {
            $score = 0;
        }

        $this->user_id = Auth()->id();
        $user = User::findOrFail($this->user_id);
        $user_exam = $user->whereHas('exams', function (Builder $query) {
            $query->where('exam_id', $this->exam_id)->where('user_id', $this->user_id);
        })->count();

        // Ambil history_answer yang sudah ada
        $existingAnswers = $user->exams()->where('exam_id', $this->exam_id)->where('user_id', $this->user_id)->pluck('history_answer')->first();

        if ($user_exam == 0) {
            // Jika tidak ada data, tambahkan baru
            $user->exams()->attach($this->exam_id, ['history_answer' => json_encode($this->selectedAnswers), 'score' => $score]);
        } else {
            // Jika sudah ada data, gabungkan selectedAnswers dengan existingAnswers
            $existingAnswersArray = $existingAnswers ? json_decode($existingAnswers, true) : [];

            // Loop melalui selectedAnswers dan update jawaban pada pertanyaan yang sudah ada di existingAnswers
            foreach ($this->selectedAnswers as $key => $value) {
                $questionId = explode('-', $value)[0];
                if (array_key_exists($questionId, $existingAnswersArray)) {
                    $existingAnswersArray[$questionId] = $value;
                }
            }

            $user->exams()->updateExistingPivot($this->exam_id, ['history_answer' => json_encode($existingAnswersArray), 'score' => $score]);
        }
        $currentPage = $this->questions()->currentPage();
        $nextPage = $currentPage + 1;
    
        // Redirect ke halaman berikutnya dengan menyertakan selectedAnswers pada URL
        return redirect()->route('exams.start', ['id' => $this->exam_id, 'page' => $nextPage, 'selectedAnswers' => json_encode($this->selectedAnswers)]);
    }
    public function render()
    {
        $exam = Exam::findOrFail($this->exam_id);
        $questions = $this->questions();

        // Mengambil selectedAnswers dari URL jika tersedia
        $selectedAnswersFromURL = request()->query('selectedAnswers');

        // Jika selectedAnswers tersedia dalam URL, gunakan nilai tersebut
        // Jika tidak, gunakan nilai dari properti $selectedAnswers
        $this->selectedAnswers = $selectedAnswersFromURL ? json_decode($selectedAnswersFromURL, true) : $this->selectedAnswers;

        return view('livewire.quiz', [
            'exam'            => $exam,
            'questions'       => $questions,
            'video'           => new Video(),
            'audio'           => new Audio(),
            'document'        => new Document(),
            'image'           => new Image(),
            'selectedAnswers' => $this->selectedAnswers,
        ]);
    }

    
}
