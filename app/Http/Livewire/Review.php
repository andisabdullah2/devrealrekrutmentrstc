<?php

namespace App\Http\Livewire;

use App\Models\Exam;
use App\Models\User;
use App\Models\Audio;
use App\Models\Exam_User;
use App\Models\Question;
use App\Models\ExamSubject;
use App\Models\Image;
use App\Models\Video;
use Livewire\Component;
use App\Models\Document;
use Livewire\WithPagination;

class Review extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $user_id;
    public $exam_id;
    public $selectedAnswers = [];
    public $total_question;

    public function mount($user_id, $exam_id)
    {
        $this->user_id = $user_id;
        $this->exam_id = $exam_id;
        $user = User::findOrfail($user_id);
        $user_exam = $user->exams->find($exam_id);
        $answer = $user_exam->pivot->history_answer;

        $result = json_decode($answer);
        $this->selectedAnswers = (array)$result;
    }

    public function questions()
    {
        $currentUser = User::findOrFail(Auth()->id());
        $exam = Exam::findOrFail($this->exam_id);
        $idUser = $currentUser->id;
        $exam_user = Exam_User::where('exam_id', $exam->id)->where('user_id', $idUser)->pluck('exam_subject_id')->toArray();
        $exam_subject = ExamSubject::where('exam_id', $exam->id)->where('id', $exam_user)->pluck('subject_id')->toArray();
        $exam_questions = Question::where('subject_id', $exam_subject);
    
        $this->total_question = $exam_questions->count();
    
        if ($this->total_question >= $exam->total_question) {
            $questions = $exam_questions->take($exam->total_question)->paginate(1);
        } elseif ($this->total_question < $exam->total_question) {
            $questions = $exam_questions->take($this->total_question)->paginate(1);
        }
    
        return $questions;
    }

    public function getAnswers()
    {
        
    }

    public function render()
    {
        return view('livewire.review', [
            'exam'      => Exam::findOrFail($this->exam_id),
            'questions' => $this->questions(),
            'video'     => new Video(),
            'audio'     => new Audio(),
            'document'  => new Document(),
            'image'     => new Image()
        ]);
    }
}
