<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\Exam_User;


class PengumumanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pengumuman.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserDetails(Request $request)
    {
        $nomorTes = $request->input('nomorTes');

        $user = User::join('profile', 'users.id', '=', 'profile.user_id')
            ->where('profile.nomorTes', $nomorTes) ->first();

        return response()->json($user);
    }
}
