<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;

class Profiles extends Controller
{
     /**
     * __construct
     *
     * @return void
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = User::findOrFail(Auth()->id());
        $profile = Profile::where('user_id',$currentUser->id)->first();
        return view('profile.index', compact('currentUser','profile'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = Profile::where('user_id', $request->input('user_id'))->first();

        if ($profile) {
            // Jika profil pengguna sudah ada, lakukan pembaruan (update)
            $profile->update([
                'tgl_lahir'        => $request->input('tgl_lahir'),
                'alamat'      => $request->input('alamat'),
                'pendidikan'      => $request->input('pendidikan'),
                'jenis_kelamin'      => $request->input('jenis_kelamin'),
                'agama'      => $request->input('agama'),
                'nomorTes'      => $request->input('nomorTes'),
                'no_hp'      => $request->input('no_hp'),
                'nik'      => $request->input('nik'),
                'nama_wali'      => $request->input('nama_wali'),
                'no_hpWali'   => $request->input('no_hpWali'),
            ]);

            if ($profile) {
                // Redirect dengan pesan sukses
                return redirect()->route('profile')->with(['success' => 'Data Berhasil Diperbarui!']);
            } else {
                // Redirect dengan pesan error
                return redirect()->route('profile')->with(['error' => 'Data Gagal Diperbarui!']);
            }
        } else {
            // Jika profil pengguna belum ada, lakukan pembuatan baru
            $profile = Profile::create([
                'user_id'    => $request->input('user_id'),
                'tgl_lahir'        => $request->input('tgl_lahir'),
                'alamat'      => $request->input('alamat'),
                'pendidikan'      => $request->input('pendidikan'),
                'jenis_kelamin'      => $request->input('jenis_kelamin'),
                'agama'      => $request->input('agama'),
                'nomorTes'      => $request->input('nomorTes'),
                'no_hp'      => $request->input('no_hp'),
                'nik'      => $request->input('nik'),
                'nama_wali'      => $request->input('nama_wali'),
                'no_hpWali'   => $request->input('no_hpWali'),
            ]);

            if ($profile) {
                // Redirect dengan pesan sukses
                return redirect()->route('profile')->with(['success' => 'Data Berhasil Disimpan!']);
            } else {
                // Redirect dengan pesan error
                return redirect()->route('profile')->with(['error' => 'Data Gagal Disimpan!']);
            }
        }
    }

}
