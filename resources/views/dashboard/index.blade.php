@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
      <br>
        <div class="header">
            <h3>Dashboard</h3>
        </div>
        <br>
        @hasanyrole('admin')
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                  <i class="fa fa-book-open text-white fa-2x"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>EXAMS</h4>
                  </div>
                  <div class="card-body">
                    {{ App\Models\Exam::count() ?? '0' }}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                  <i class="fa fa-bell text-white fa-2x"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>QUESTIONS</h4>
                  </div>
                  <div class="card-body">
                    {{ App\Models\Question::count() ?? '0' }}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                  <i class="fa fa-tags text-white fa-2x"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>SUBJECTS</h4>
                  </div>
                  <div class="card-body">
                    {{ App\Models\Subject::count() ?? '0' }}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                  <i class="fa fa-users text-white fa-2x"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>PESERTA</h4>
                  </div>
                  <div class="card-body">
                    {{ App\Models\User::role('peserta')->count() ?? '0' }}
                  </div>
                </div>
              </div>
            </div>                  
          </div>
        @endhasanyrole

        @hasanyrole('penguji')
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                  <i class="fa fa-tags text-white fa-2x"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>SUBJECTS</h4>
                  </div>
                  <div class="card-body">
                    {{ App\Models\Subject::count() ?? '0' }}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                  <i class="fa fa-users text-white fa-2x"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>PESERTA</h4>
                  </div>
                  <div class="card-body">
                    {{ App\Models\User::role('peserta')->count() ?? '0' }}
                  </div>
                </div>
              </div>
            </div>                  
          </div>
        @endhasanyrole
        @hasrole('peserta')
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-primary">
                <i class="fa fa-book-open text-white fa-2x"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>MY EXAMS</h4>
                </div>
                <div class="card-body">
                  {{ $exams->count() ?? '0' }}
                </div>
              </div>
            </div>
          </div>

        </div>
        <h4>Petunjuk Penggunaan</h4>
              <p>
                <h5>Panduan Umum</h5>
                1. Pastikan Nama Dan Email Sudah Sesuai Ketika Pendaftaran Administrasi <br>
                2. Disarankan Untuk Melengkapi Profile Pengguna <br>
                3. Soal Ujian Berada Di Menu Exam ( Hanya Tampil Ketika Peserta Telah Di Approve Oleh Admin Bersangkutan) <br>
                4. Exam Soal Hanya Tampil Sesuai Sesi Yang Ditentukan <br>
                5. Soal Di Acak Dan Tidak Akan Sama Dengan Peserta Lain <br>
                6. Soal Yang Benar Bernilai 1 Dan Yang Salah Bernilai 0 (Tidak Ada Mines Jika Jawaban Salah)
              </p>
              <br>
              <p>
                <h5>Panduan Pengerjaan</h5>
                1. Pada Menu Exam Perhatikan Tabel <i>Akses</i> Dengan Icon Mata <br>
                2. Klik Icon Tersebut Untuk Masuk Ke-Detai Test <br>
                3. Terdapat Beberapa Ketentuan : <br>
                <ul>
                  <li>Button Start Hanya Aktif Ketika Waktu Test Sudah Di Buka</li>
                  <li>Button Start Akan Disable Ketika Selesai Pengerjaan Test Atau Waktu Test Sudah Berakhir</li>
                </ul>
                4. Klik Button Start Untuk Memulai Test <br>
                5. Untuk Memilih Jawaban, Pastikan Pilihan Jawaban Telah Berubah Berwarna Hijau Sebelum Melanjutkan Ke Soal Berikutnya<br>
                6. Jika Kesulitan Menjawab Soal, Soal Dapat Di Skip Dengan Menggunakan Navigasi Yang Ada di Bawah Soal (Begitupun Untuk Kembali Ke Soal Sebelumnya Yang Belum Terjawab) <br>
                7. Ketika Selesai Mengerjakan Test Pastikan Menekan Tombol Submit Yang Ada Dihalaman Akhir Test
              </p>
              <br>
              <p>
                <h5>Panduan Penyelesaian</h5>
                1. Pastikan Semua Jawaban Sudah Terjawab <br>
                2. Ketika Selesai Mengerjakan Test Pastikan Menekan Tombol Submit Yang Ada Dihalaman Akhir Test <br>
                3. Silahkan Logout Dan Matikan Koneksi Wifi Yang Terhubung
              </p>

              <h1>Selamat Mengerjakan</h1>
        @endhasanyrole
    </section>
</div>
<style>
  .section-header{
    background-color: transparent;
  }
</style>
@endsection