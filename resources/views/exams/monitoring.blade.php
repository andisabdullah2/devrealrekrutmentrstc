@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
        <br>
        <div class="section-body">
            <input type="hidden" name="akhirTest" id="akhirTest" value="{{$exams->end}}">
            <div>
                @livewire('monitoring', ['id' => $id])
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function(){
        setTimeout(function() {
            location.reload();
        }, 12000);
    })
</script>



@stop