@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
        <br>
        <div class="header">
            <h3>Monitoring</h3>
        </div>
        <br>

        <div class="section-body">
            <div>
                @livewire('monitoring-exam-berakhir')
            </div>
        </div>
    </section>
</div>

@stop