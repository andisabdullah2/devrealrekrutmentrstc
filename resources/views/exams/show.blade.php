@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
        <br>
        <div class="header">
            <h3>Exam Detail</h3>
        </div>
        <br>

        <div class="section-body">

            <div class="card">
                <div class="card-header">
                    <h4><i class="fas fa-exam"></i> Ujian {{ $exam->name }} </h4>
                </div>

                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Durasi Ujian : {{ $exam->time }} Menit</li>
                        <li class="list-group-item">Jumlah Soal : {{ $questionCount}}</li>
                        <li class="list-group-item">Ujian dibuka : {{ TanggalID($exam->start) }}</li>
                        <li class="list-group-item">Ujian ditutup : {{ TanggalID($exam->end) }}</li>
                    </ul>
                </div>
                <div class="card-footer">
                    @if ($exam_user1->finish == '1')
                    <a onclick="goBack()" class="btn btn-danger btn-lg btn-block" role="button" aria-pressed="true">ANDA TELAH MENGERJAKAN TEST - KEMBALI</a>
                    @elseif (now() < $exam->start)
                    <a onclick="goBack()" class="btn btn-warning btn-lg btn-block" role="button" aria-pressed="true">TEST BELUM DIBUKA - KEMBALI</a>
                    @elseif(now() > $exam->end)
                    <a onclick="goBack()" class="btn btn-danger btn-lg btn-block" role="button" aria-pressed="true">TEST SUDAH DITUTUP - KEMBALI</a>
                    @elseif (now() > $exam->start && now()  < $exam->end)
                    <a href="{{ route('exams.start', $exam->id) }}" class="btn btn-primary btn-lg btn-block" role="button" aria-pressed="true">START</a>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    function goBack() {
    window.history.back();
}
</script>

@stop