@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
        <br>
        <div class="header">
            <h3>Tambah Exam</h3>
        </div>
        <br>
        <div class="section-body">

            <div class="card">
                <div class="card-header">
                    <h4><i class="fas fa-exam"></i> Tambah Exam</h4>
                </div>

                <div class="card-body">
                    <form action="{{ route('exams.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label>NAME</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" >
                            @error('name')
                            <div class="invalid-feedback" style="display: block">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                                    <label>JENIS TEST</label>
                                    <select class="form-control select-video @error('video_id') is-invalid @enderror" name="jenisTest">
                                        <option value="">- SELECT JENIS -</option>
                                            <option value="SESI 1">SESI 1</option>
                                            <option value="SESIS 2">SESI 2</option>
                                            <option value="SESIS 3">SESI 3</option>
                                            <option value="SESIS 4">SESI 4</option>
                                            <option value="SESIS 5">SESI 5</option>
                                            <option value="SESIS 6">SESI 6</option>
                                    </select>
                                    @error('video_id')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                         </div>

                        <div class="form-group">
                            <label>TIME (MINUTE)</label>
                            <input type="number" name="time" value="{{ old('time') }}" class="form-control" >

                            @error('time')
                            <div class="invalid-feedback" style="display: block">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>JUMLAH JENIS SOAL</label>
                            <input type="number" name="total_question" value="{{ old('total_question') }}" class="form-control" >

                            @error('total_question')
                            <div class="invalid-feedback" style="display: block">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>START</label>
                            <input type="datetime-local" name="start" value="<?= date('Y-m-d', time()); ?>" class="form-control @error('start') is-invalid @enderror">

                            @error('start')
                            <div class="invalid-feedback" style="display: block">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>END</label>
                            <input type="datetime-local" name="end" value="<?= date('Y-m-d', time()); ?>" class="form-control @error('end') is-invalid @enderror">

                            @error('end')
                            <div class="invalid-feedback" style="display: block">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <livewire:question-checklist />


                        <button class="btn btn-primary mr-1 btn-submit" type="submit"><i class="fa fa-paper-plane"></i>
                            SIMPAN</button>
                        <button class="btn btn-warning btn-reset" type="reset"><i class="fa fa-redo"></i> RESET</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

@stop
