@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
        <br>
        <div class="header">
            <h3>Selamat Mengerjakan</h3>
        </div>
        <br>
        <div class="section-body">    
            @livewire('quiz', ['id' => $id])
            
        </div>
    </section>
</div>
@stop





