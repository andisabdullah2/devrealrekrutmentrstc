@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
        <br>
        <div class="header">
            <h3>Review</h3>
        </div>
        <br>

        

        <div class="section-body">
            
            
            @livewire('review', ['user_id' => $userId, 'exam_id' => $examId])
            
        </div>
    </section>
</div>
@stop





