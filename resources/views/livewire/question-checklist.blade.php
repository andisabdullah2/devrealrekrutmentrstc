<div>
  @if(isset($selectedQuestion))
    <div class="form-group">
      <label>Jenis Soal Terpilih</label>
      <table id="table_id" class="table table-bordered table-hover">
        <thead>
        <tr>
          <th>Delete</th>
          <th>Jenis Soal</th>
          <th>Jumlah Soal</th>
          <th>Checked</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($questionsAll))
            @foreach ($questionsAll as $subject)
            <tr>
              <td>
                  <button type="button" class="btn btn-danger" wire:click="deselectQuestion({{ $subject->id }})"><i class="fas fa-minus-circle"></i></button>
              </td>
              <td>{{ $subject->name }}</td>
              <td>{{ $question->getJumlah($subject->id) }}</td>
              <td><input class="form-check-input" type="checkbox" name="subject[]" value="{{ $subject->id }}" 
                id="check-{{ $subject->id }}" }} checked></td>
            </tr>
            @endforeach
        @endif
        
        </tbody>
      </table>
    </div>
  @endif
    <div class="card">
        <div class="card-header">
            <h3>Tambah Jenis Soal</h3>
        </div>
        <div class="card-body">
          {{-- @foreach ($selectedQuestion as $item)
              {{ $item  }}
          @endforeach --}}
                
          <table id="table_id" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>Pilih</th>
              <th>Jenis Soal</th>
              <th>Jumlah Soal</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($subjects as $subject)
              <tr>
                <td>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" wire:model="selectedQuestion" type="checkbox" name="subject[]" value="{{ $subject->id }}" 
                               id="check-{{ $subject->id }}" }}>
                    </div>
                </td>
                <td>{{ $subject->name}}</td>
                <td>{{ $question->getJumlah($subject->id) }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
         
          <div style="text-align: center">
            {{$subjects->links()}}
          </div>
        </div>
    </div>
</div>
