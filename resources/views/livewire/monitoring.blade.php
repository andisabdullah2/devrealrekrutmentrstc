<style>
#contain {
  overflow-y: scroll;  
}
#table_scroll {
  width: 100%;
}
#table_scroll tbody td {
  padding: 10px;
}
#table_fixed thead th {
  padding: 10px;
  background-color: #b90be0;
  color: #fff;
  font-weight: 100;
}
</style>

<script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
            <div class="card">
                <div class="card-header">
                    <h4><i class="fas fa-exam"></i> Monitoring</h4>
                </div>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#monitoringTab">Monitoring Sesi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#allDataTab">All Data</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Monitoring Tab -->
                    <div id="monitoringTab" class="tab-pane fade show active">
                        <div class="card-body">
                                <div class="table-responsive">
                                    <table border="0" id="table_fixed">
                                        <thead>
                                        <tr>      
                                                <th scope="col">NO.</th>
                                                <th scope="col">NAMA PESERTA</th>
                                                <th scope="col">NAMA TES</th>
                                                <th scope="col">JENIS SOAL</th>
                                                <th scope="col">SCORE SEMENTARA</th>
                                                <th scope="col">SCORE AKHIR</th>
                                                <th scope="col">PERINGKAT</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <div id="contain_all">  
                                        <table border="0" id="table_scroll">
                                                <tbody>
                                                    @foreach ($exams as $key => $exam)
                                                    <tr>
                                                        <td width="1%">{{ $key + 1 }}</td>
                                                        <td>{{ $exam->user_name }}</td>
                                                        <td>{{ $exam->name }}</td>
                                                        <td>{{ $exam->subject_name }}</td>
                                                        <td>
                                                            <span style="color: orange;">
                                                                {{ $exam->score_sementara !== null ? $exam->score_sementara : 'Test Belum Dimulai / Berakhir' }}
                                                            </span>
                                                        </td>                                    
                                                        <td>
                                                            <span style="color: green">
                                                            {{ $exam->score_akhir !== null ? $exam->score_akhir : 'Test Sementara Berlangsung / Belum Dimulai' }}
                                                            </span>
                                                        </td>
                                                        <td>{{ $key + 1 }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- All Data Tab -->
                    <div id="allDataTab" class="tab-pane fade">

                        <div class="card-body">
                                <div class="table-responsive">
                                    <table border="0" id="table_fixed_all">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="text-align: center;width: 6%">NO.</th>
                                                <th scope="col">NAMA PESERTA</th>
                                                <th scope="col">NAMA TES</th>
                                                <th scope="col">JENIS SOAL</th>
                                                <th scope="col">SCORE SEMENTARA</th>
                                                <th scope="col">SCORE AKHIR</th>
                                                <th scope="col">PERINGKAT</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div id="contain">  
                                        <table border="0" id="table_scroll_all">
                                            <tbody>
                                                @foreach ($examskeseluruhan_result as $key1 => $examskeseluruhan)
                                                    <tr>
                                                        <td>{{ $key1 + 1 }}</td>
                                                        <td>{{ $examskeseluruhan->user_name }}</td>
                                                        <td>{{ $examskeseluruhan->name }}</td>
                                                        <td>{{ $examskeseluruhan->subject_name }}</td>
                                                        <td>
                                                            <span style="color: orange;">
                                                                {{ $examskeseluruhan->score_sementara !== null ? $examskeseluruhan->score_sementara : 'Test Belum Dimulai / Berakhir' }}
                                                            </span>
                                                        </td>                                    
                                                        <td>
                                                            <span style="color: green">
                                                            {{ $examskeseluruhan->score_akhir !== null ? $examskeseluruhan->score_akhir : 'Test Sementara Berlangsung / Belum Dimulai' }}
                                                            </span>
                                                        </td>
                                                        <td>{{ $key1 + 1 }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

<script>
    $(document).ready(function() {
  
    pageScroll();
    $("#contain").mouseover(function() {
        clearTimeout(my_time);
    }).mouseout(function() {
        pageScroll();
    });
    $("#contain_all").mouseover(function() {
        clearTimeout(my_time);
    }).mouseout(function() {
        pageScroll();
    });
    
    getWidthHeader('table_fixed','table_scroll');
    getWidthHeader('table_fixed_all','table_scroll_all');

    
    });

    var my_time;
    function pageScroll() {
        var objDiv = document.getElementById("contain");
    objDiv.scrollTop = objDiv.scrollTop + 1;  
    if ((objDiv.scrollTop + 100) == objDiv.scrollHeight) {
        objDiv.scrollTop = 0;
    }
    my_time = setTimeout('pageScroll()', 25);
    }

    function getWidthHeader(id_header, id_scroll) {
    var colCount = 0;
    $('#' + id_scroll + ' tr:nth-child(1) td').each(function () {
        if ($(this).attr('colspan')) {
        colCount += +$(this).attr('colspan');
        } else {
        colCount++;
        }
    });
    
    for(var i = 1; i <= colCount; i++) {
        var th_width = $('#' + id_scroll + ' > tbody > tr:first-child > td:nth-child(' + i + ')').width();
        $('#' + id_header + ' > thead th:nth-child(' + i + ')').css('width',th_width + 'px');
    }
    }

    var my_time_all;
    function pageScroll() {
        var objDiv = document.getElementById("contain_all");
    objDiv.scrollTop = objDiv.scrollTop + 1;  
    if ((objDiv.scrollTop + 100) == objDiv.scrollHeight) {
        objDiv.scrollTop = 0;
    }
    my_time_all = setTimeout('pageScroll()', 25);
    }

    function getWidthHeader(id_header, id_scroll) {
    var colCount = 0;
    $('#' + id_scroll + ' tr:nth-child(1) td').each(function () {
        if ($(this).attr('colspan')) {
        colCount += +$(this).attr('colspan');
        } else {
        colCount++;
        }
    });
    
    for(var i = 1; i <= colCount; i++) {
        var th_width = $('#' + id_scroll + ' > tbody > tr:first-child > td:nth-child(' + i + ')').width();
        $('#' + id_header + ' > thead th:nth-child(' + i + ')').css('width',th_width + 'px');
    }
    }

</script>

