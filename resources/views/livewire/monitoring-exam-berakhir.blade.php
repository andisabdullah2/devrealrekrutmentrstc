<div class="card">
                <div class="card-header">
                    <h4><i class="fas fa-exam"></i> Monitoring</h4>
                </div>

                <div class="card-body">
                    <form action="{{ route('exams.index') }}" method="GET">
                        @hasanyrole('penguji|admin')
                        <div class="form-group">
                            <div class="input-group mb-3">
                                @can('exams.create')
                                    <div class="input-group-prepend">
                                        <a href="{{ route('exams.create') }}" class="btn btn-primary" style="padding-top: 10px;"><i class="fa fa-plus-circle"></i> TAMBAH</a>
                                    </div>
                                @endcan
                                <input type="text" class="form-control" name="q"
                                       placeholder="cari berdasarkan nama exam">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> CARI
                                    </button>
                                </div>
                            </div>
                        </div>
                        @endhasanyrole
                    </form>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center;width: 6%">NO.</th>
                                <th scope="col">NAMA PESERTA</th>
                                <th scope="col">NAMA TES</th>
                                <th scope="col">JENIS SOAL</th>
                                <th scope="col">SCORE SEMENTARA</th>
                                <th scope="col">SCORE AKHIR</th>
                                <th scope="col">PERINGKAT</th>
                                <th scope="col" style="width: 15%;text-align: center">AKSI</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($examstidakaktif as $key => $exam)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $exam->user_name }}</td>
                                    <td>{{ $exam->name }}</td>
                                    <td>{{ $exam->subject_name }}</td>
                                    <td>{{ $exam->score_sementara !== null ? $exam->score_sementara : 'Test Belum Dimulai / Berakhir' }}</td>
                                    <td>{{ $exam->score_akhir !== null ? $exam->score_akhir : 'Test Sementara Berlangsung / Belum Dimulai' }}</td>
                                    <td>{{ $key + 1 }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="text-align: center">
                        </div>
                    </div>
                </div>
            </div>