<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Pengumuman &mdash; Rekrutment RSTC</title>

    <link rel="shortcut icon" href="{{ asset('assets/img/schools.svg') }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-social/bootstrap-social.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <style>
        body {
            background-image: url('{{ asset("assets/img/background.jpg") }}');
            background-size: cover;
            background-repeat: no-repeat;
        }
        .card {
            background-color:#14CE9E;
        }
        .card h4{
            color:white;
        }
           /* Modal header styles for Congratulations (green) and Mohon Maaf (red) */
        .modal-header.congratulations-modal {
            background-color: #28a745;
            color: #fff;
        }
        .modal-header.mohon-maaf-modal {
            background-color: #dc3545;
            color: #fff;
        }

        /* Modal body text color for Congratulations (green) and Mohon Maaf (red) */
        .modal-body.congratulations-modal {
            color: #28a745;
        }
        .modal-body.mohon-maaf-modal {
            color: #dc3545;
        }
    </style>
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-xl-6 offset-xl-3">
                        <div class="card shadow">
                            <div class="card-header">
                                <center><h4>Pengumuman Kelulusan Rekrutmen RSTC</h4></center>
                            </div>
                            <div class="card-body">
                                <form id="result-form">
                                @csrf
                                    <div class="form-group">
                                        <label for="nomorTes">Masukkan Nomor Test Anda:</label>
                                        <input type="text" class="form-control" id="nomorTes" name="nomorTes" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block">Lihat Hasil Pengumuman</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-xl-8 offset-xl-2">
                        <div class="card">
                            <div class="card-header">
                                <center><h4>Details</h4></center>
                            </div>
                            <div class="card-body" id="user-details-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Modal for Congratulations -->
    <div class="modal fade" id="congratulationsModal" tabindex="-1" role="dialog" aria-labelledby="congratulationsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header congratulations-modal">
                    <h5 class="modal-title" id="congratulationsModalLabel">Congratulations!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body congratulations-modal">
                    <p>Selamat Anda Telah Lulus. Mohon Selalu Memerhatikan Update Info Terbaru </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Mohon Maaf -->
    <div class="modal fade" id="mohonMaafModal" tabindex="-1" role="dialog" aria-labelledby="mohonMaafModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header mohon-maaf-modal">
                    <h5 class="modal-title" id="mohonMaafModalLabel">Belum Lulus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mohon-maaf-modal">
                    <p>Mohon maaf, Anda tidak dapat melanjutkan ke tahap berikutnya.</p>
                </div>
            </div>
        </div>
    </div>


    <!-- General JS Scripts -->
    <script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/modules/popper.js') }}"></script>
    <script src="{{ asset('assets/modules/tooltip.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/modules/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>

    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function () {
            $('#result-form').on('submit', function (e) {
                e.preventDefault();

                var nomorTes = $('#nomorTes').val();

                // Perform AJAX call to fetch the user details from the server
                $.ajax({
                    url: '/getUserDetails', // The URL of the route defined in web.php
                    method: 'POST',
                    data: {
                        nomorTes: nomorTes
                    },
                    success: function (response) {
                        // Display the user details in the card body
                        if (response) {
                            const status = response.status.toLowerCase();
                            const statusText = status === 'lulus' ? 'Congratulations!' : 'Status: ' + status;

                            $('#user-details-body').html(`
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama">Nama:</label>
                                            <input type="text" class="form-control" id="nama" name="nama" value="${response.name}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="nomorTes">NIK:</label>
                                            <input type="text" class="form-control" id="nomorTes" name="nomorTes" value="${response.nomorTes}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="status">Status:</label>
                                            <input type="text" class="form-control" id="status" name="status" value="${response.status}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nik">Nomor Tes:</label>
                                            <input type="text" class="form-control" id="nik" name="nik" value="${response.nik}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="tgl_lahir">Tanggal Lahir:</label>
                                            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value="${response.tgl_lahir}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label for="nomorhp">Nomor Hp:</label>
                                            <input type="text" class="form-control" id="stanomorhptus" name="nomorhp" value="${response.no_hp}" disabled>
                                        </div>
                                    </div>
                                </div>
                            `);

                            // Show pop-up for "lulus" status
                            if (status === 'lulus') {
                            showCongratulationsPopup();
                            } else {
                                showMohonMaafPopup();
                            }
                        } else {
                            $('#user-details-body').html('<p>No user found with the given test number.</p>');
                        }
                    },
                    error: function () {
                        // Handle error scenarios if necessary
                        $('#user-details-body').html('<p>Failed to fetch user details. Please try again later.</p>');
                    }
                });
            });
        });
        function showCongratulationsPopup() {
            $('#congratulationsModal').modal('show');
        }
        function showMohonMaafPopup() {
            $('#mohonMaafModal').modal('show');
        }
    </script>

    <!-- Template JS File -->
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>
