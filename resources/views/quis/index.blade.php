@extends('layouts.app')

@section('content')

<div class="main-content">
    <section class="section">
        <br>
        <div class="header">
            <h3>Selamat Mengerjakan</h3>
        </div>
        <br>
        <div class="section-body">    
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-12">
                            <h4><i class="fas fa-exam"></i> {{ $exam['name'] }} </h4>
                        </div>
                        <div class="col-12">
                            <!-- <span class="badge badge-danger" id="timer"></span> -->
                        </div>
                    </div>
                </div>
                {{$questions}}
                @foreach ($questions as $question)
                <div class="card-body">
                    <p>Soal No. {{ $questions->currentPage() }}</p>
                    <p><b>{{ $question['detail'] }}</b></p>
                        @if ($question['video_id'])
                            <video width="320" height="240" controls>
                                <source src="{{ Storage::url('public/videos/'.$video->getLink($question['video_id'])) }}" type="video/mp4">
                                <source src="{{ Storage::url('public/videos/'.$video->getLink($question['video_id'])) }}" type="video/mpeg">
                            </video>
                        @elseif($question['audio_id'])
                            <audio width="160" height="120" controls>
                                <source src="{{ Storage::url('public/audios/'.$audio->getLink($question['audio_id'])) }}" type="audio/mp3">
                                <source src="{{ Storage::url('public/audios/'.$audio->getLink($question['audio_id'])) }}" type="audio/wav">
                            </audio>
                        @elseif($question['document_id'])
                            <a href="{{ Storage::url('public/documents/'.$document->getLink($question['document_id'])) }}">DOCUMENT</a>
                        @elseif($question['image_id'])
                        <img src="{{ Storage::url('public/images/'.$image->getLink($question['image_id'])) }}" style="width: 600px">
                        @else
                        @endif
                    <br>
                    <i>Pilih salah satu jawaban dibawah ini:</i> 
                    <br>
                    <div class="btn-group-vertical" role="group" aria-label="Basic example">
                        <br>
                        <div style="display: flex; align-items: center;">
                            <button type="button" onclick="selectAnswer({{ $question['id'] }}, 'A')" id="{{ $question['id'].'-A' }}"
                                class="{{ in_array($question['id'].'-A', $selectedAnswers) ? 'btn btn-success border border-secondary rounded' : 'btn btn-light border border-secondary rounded' }}">
                                <p class="text-left"><b> A. {{ $question['option_A'] }} </b></p>
                            </button>
                            @if ($question['imageA'] !== null)
                                <img src="{{ Storage::url('public/images/' . $question['imageA']) }}" alt="Image A" style="width: 100px; height: 100px; margin-left: 10px;">
                            @endif
                        </div>
                        <br>
                        <div style="display: flex; align-items: center;">
                            <button type="button" onclick="selectAnswer({{ $question['id'] }}, 'B')" id="{{ $question['id'].'-B' }}"
                                class="{{ in_array($question['id'].'-B', $selectedAnswers) ? 'btn btn-success border border-secondary rounded' : 'btn btn-light border border-secondary rounded' }}">
                                <p class="text-left"><b> B. {{ $question['option_B'] }} </b></p>
                            </button>
                            @if ($question['imageB'] !== null)
                                <img src="{{ Storage::url('public/images/' . $question['imageB']) }}" alt="Image B" style="width: 100px; height: 100px; margin-left: 10px;">
                            @endif
                        </div>
                        <br>
                        <div style="display: flex; align-items: center;">
                            <button type="button" onclick="selectAnswer({{ $question['id'] }}, 'C')" id="{{ $question['id'].'-C' }}"
                                class="{{ in_array($question['id'].'-C', $selectedAnswers) ? 'btn btn-success border border-secondary rounded' : 'btn btn-light border border-secondary rounded' }}">
                                <p class="text-left"><b> B. {{ $question['option_C'] }} </b></p>
                            </button>
                            @if ($question['imageB'] !== null)
                                <img src="{{ Storage::url('public/images/' . $question['imageC']) }}" alt="Image B" style="width: 100px; height: 100px; margin-left: 10px;">
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach

                {{-- Pagination links --}}
                <div class="d-flex justify-content-center">
                    {{ $questions->appends(['selectedAnswers' => json_encode($selectedAnswers)])->links() }}
                </div>
                <div class="card-footer">
                    @if ($exam->jenisTest == 'TKD')
                        @if ($questions->currentPage() == $questions->lastPage())
                            <button onclick="submitAnswers()" class="btn btn-success btn-lg btn-block">Submit</button>
                        @else
                            <button onclick="nextAnswers()" class="btn btn-primary btn-lg btn-block">Next</button>
                        @endif
                    @else
                        @if ($questions->currentPage() == $questions->lastPage())
                            <button onclick="submitAnswersAkhlak()" class="btn btn-success btn-lg btn-block">Submit</button>
                        @else
                            <button onclick="nextAnswersAkhlak()" class="btn btn-secondary btn-lg btn-block">Next</button>
                        @endif
                    @endif
                </div>
            </div>

        </div>
    </section>
</div>
@stop

<script>
 
    // Function to select an answer
    function selectAnswer(questionId, option) {
        const answer = questionId + '-' + option;
        const button = document.getElementById(questionId + '-' + option);

        if (button.classList.contains('btn-success')) {
            // If the button is already selected, unselect it
            button.classList.remove('btn-success');
            button.classList.add('btn-light');
            const index = selectedAnswers.indexOf(answer);
            if (index !== -1) {
                selectedAnswers.splice(index, 1);
            }
        } else {
            // If the button is not selected, select it
            button.classList.remove('btn-light');
            button.classList.add('btn-success');
            selectedAnswers.push(answer);
        }
    }

    // Function to submit the answers
    function submitAnswers() {
        // Implement the logic to submit the answers
        // ...
    }

    // Function to go to the next question
    function nextAnswers() {
        // Implement the logic to move to the next question
        // ...
    }

    // Function to submit the Akhlak answers
    function submitAnswersAkhlak() {
        // Implement the logic to submit the Akhlak answers
        // ...
    }

    // Function to go to the next Akhlak question
    function nextAnswersAkhlak() {
        // Implement the logic to move to the next Akhlak question
        // ...
    }
</script>




