@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
        <br>
        <div class="header">
            <h3>Profile </h3>
        </div>
        <br>

        <div class="section-body">

            <div class="card">
                <div class="card-body">
                    <form action="{{ route('update-profile') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$currentUser->id}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>NAMA</label>
                                    <input type="text" name="name" value="{{$currentUser->name}}" class="form-control" >
                                    @error('name')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>TANGGAL LAHIR</label>
                                    <input type="date" name="tgl_lahir" value="{{optional($profile)->tgl_lahir }}" class="form-control" >
                                    @error('tgl_lahir')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>ALAMAT</label>
                                    <input type="text" name="alamat" value="{{optional($profile)->alamat }}" class="form-control" >

                                    @error('alamat')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>PENDIDIKAN</label>
                                    <input type="text" name="pendidikan" value="{{optional($profile)->pendidikan }}" class="form-control" >

                                    @error('pendidikan')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>JENIS KELAMIN</label>
                                    <input type="text" name="jenis_kelamin" value="{{optional($profile)->jenis_kelamin }}" class="form-control" >

                                    @error('jenis_kelamin')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>AGAMA</label>
                                    <input type="text" name="agama" value="{{optional($profile)->agama }}" class="form-control" >

                                    @error('agama')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>nomorTes</label>
                                    <input type="text" name="nomorTes" value="{{optional($profile)->nomorTes }}" class="form-control" >
                                    @error('nomorTes')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>NOMOR HP</label>
                                    <input type="text" name="no_hp" value="{{optional($profile)->no_hp }}" class="form-control" >
                                    @error('no_hp')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" name="nik" value="{{optional($profile)->nik }}" class="form-control" >

                                    @error('nik')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>NAMA WALI</label>
                                    <input type="text" name="nama_wali" value="{{optional($profile)->nama_wali }}" class="form-control" >
                                    @error('nama_wali')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>NOMOR HP WALI</label>
                                    <input type="text" name="no_hpWali" value="{{optional($profile)->no_hpWali }}" class="form-control" >
                                    @error('no_hpwali')
                                    <div class="invalid-feedback" style="display: block">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary mr-1 btn-submit" type="submit"><i class="fa fa-paper-plane"></i>
                            SIMPAN</button>
                        <button class="btn btn-warning btn-reset" type="reset"><i class="fa fa-redo"></i> RESET</button>

                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@stop